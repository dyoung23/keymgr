### 使用方法：

#### 初始化配置
修改主机hostname，使hostname对应主机出口IP，否则获取本机IP的代码会获取到其他网卡的IP

#### 启动server

```
python3 server.py
```

更多信息可查看--help

#### 启动client

```
python3 client.py -l http://[sever ip]:[server port]
```

更多信息可查看--help



### 接口1：http://[ip]:[port]/users/get

- 方法类型：GET

- 参数：

  - ip：想要获取账号信息的服务器IP地址，获取全量信息传空值
  - mac：想要获取账号信息的服务器MAC地址，获取全量信息传空值

- 返回值：

  ```json
  {
  	"data":[
  		{
  			"user":xxx,"ip":xxx,……
  		},
  		……
  	],
  	"msg":"OK"
  }
  ```

  

### 接口2：http://[ip]:[port]/changepwd/<ip>/<mac>

- 方法类型：POST

- 拼接url：

  - ip：想要执行操作的服务器IP地址
  - mac：想要执行操作的服务器MAC地址

- forms参数：

  - user：想要修改密码的账户名

- 返回值：

  ```json
  {
  	"msg":"OK" //若执行失败返回"error"
  }
  ```

  

### 接口3：http://[ip]:[port]/rotatepwd/<ip>/<mac>

- 方法类型：GET

- 拼接url：

  - ip：想要执行操作的服务器IP地址
  - mac：想要执行操作的服务器MAC地址

- 参数：

  - 无

- 返回值：

  ```json
  {
  	"msg":"OK" //若执行失败返回"error"
  }
  ```

  