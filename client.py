#!/usr/bin/env python

"""
Keymgr:Password changer client for Linux user

To allow users to change their own password without superuser/root privileges.

Usage:
  client.py [-s SERVER -p port -l ServerUrl]
  client.py [-h | --help]
  client.py --version

Options:
  -h --help                 Show this screen.
  -v --version              Show version.
  -l --url ServerUrl        Server URL
  -s --server SERVER        client server host [default: 0.0.0.0]
  -p --port PORT            client server port [default: 8001].
"""

from asyncio import FastChildWatcher
from operator import methodcaller
from bottle import run, route, request, app, HTTPResponse
from docopt import docopt
import spwd
import crypt
import random
import string
import fileinput
import socket
import uuid
import datetime
import requests
import json
import os,sys

hellopath = "/hello"
createpath = "/users/create"
removepath = "/users/remove"
updatepath = "/users/update"
getpath = "/users/get"
userhandler = None
TimeOut = 3
Shadow_File = "/home/dyc/keymgr/shadow" #"/etc/shadow" #



class UserHandler(object):
    def __init__(self,url):
        self.rawusers = None
        self.rawuser = None
        self.mac = get_mac_address()
        self.ipaddr = get_ip()
        self.srvhandler = ServerHandler(url)
        res = self.srvhandler.ping()
        if res == False:
            print('Server URL is unavailable!')
            sys.exit(1)

    def getallrawusers(self):
        """Get all raw users from shadow file
        """
        self.rawusers = spwd.getspall()

    '''
    def getonerawuser(self,username=None):
        """Get a raw user from shadow file by username
        """
        try:
            self.rawuser = spwd.getspnam(username)
        except KeyError:
            return False
    '''

    def hashing(self, passwd, salt=None):
        """Hashing password with salt

        Generate a hashed value for given password and salt

        Args:
            passwd:     Password to be hashed
            salt:       (Optional) Salt to be use when hashing

        Returns:
            Hashed value

        Raises:
            None
        """
        if not salt:
            hashed = crypt.crypt(passwd, str(self.generate_salt()))
        else:
            hashed = crypt.crypt(passwd, salt)
        return hashed

    def generate_salt(self):
        """Generate salt

        Generate salt to be use with hashing()

        Args:
            None

        Returns:
            Salt value

        Raises:
            None
        """
        salt = ([random.choice(string.ascii_letters +
                               string.digits) for _ in range(16)])
        salt = "$6$" + ''.join(salt) + "$"
        return salt

    def createrandompwd(self,length=20):
        password = ''.join(
            [random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits + string.punctuation) for i in
            range(length)])
        return password

    def change_passwd(self, users):
        """Change password
        Search and replace /etc/shadow file with newly generated hashed value which is stored in new users list.
        """

        # TODO: Retrieve hashed value from DB and write to /etc/shadow ,
        #       instead of directly write
        print('in change_paswd')
        shadow_file = Shadow_File
        for user in users:
            if user["type"] == "nologin":
                continue
            prev_hashed = user['prev_hashed']
            new_hashed = user['new_hashed']
            for line in fileinput.input(shadow_file, inplace=True, backup='.bak'):
                line = line.replace(prev_hashed, new_hashed)
                print(line,end=""), 
            fileinput.close()

        root_uid = 0
        shadow_gid = 42
        os.chown(shadow_file, root_uid, shadow_gid)

    def filteruser(self,rawuser=None):
        """mark user by login type

        """
        if rawuser[1] == "*" or rawuser[1] == "!!":
            user = {"user":rawuser[0],"type":"nologin","prev_hashed":"","new_hashed":""}
        else:
            user = {"user":rawuser[0],"type":"login","prev_hashed":rawuser[1],"new_hashed":rawuser[1]}
        return user

    def startup(self):
        """first time init local users
        """
        print('in startup') #
        res = self.srvhandler.getusersbyhost(self.ipaddr,self.mac)
        if res == False:
            print('failed to get users by host:',self.ipaddr,self.mac)
            return False
        self.getallrawusers()
        if res['data']:
            #has been inited
            if self.scanupdate(res['data']) == False:
                print('failed to scanupdate in startup.')
                return False
        else:
            if self.initusers() == False:
                print('failed to init users in startup.')
                return False
    
    def scanupdate(self,dbusers=None):
        """scan inited server or changed by user manuelly,can periodly scan
        """
        print('in scanupdate') #
        if len(self.rawusers) == len(dbusers):
            #TODO if need to check pwd changed or not
            return
        else:
            # if delete or create new user
            if self.initusers() == False:
                print('failed to init users.')
                return False

    def initusers(self):
        """init
        """
        print('in initusers') #
        users = self.userslist()
        #print('init users %s' % users)
        if self.srvhandler.removeusersbyhost(self.ipaddr,self.mac) == False:
            print('failed to remove users by host in rotationpwd:',self.ipaddr,self.mac)
            return False
        if self.srvhandler.createusers(users) == False:
            print('failed to create new users.')
            return False
        else:
            self.change_passwd(users)
    
    def userslist(self):
        """creates users list
        """
        print('in userslist') #
        users = []
        for i in self.rawusers:
            user = self.userjson(i)
            users.append(user)
        return users

    def userjson(self,rawuser):
        '''create user json
        '''
        print('in userjson')
        user = self.filteruser(rawuser)
        user.update({"ip":self.ipaddr,"mac":self.mac,"update_time":datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d %H:%M:%S')})
        if user["type"] == "nologin":
            user.update({"pwd":""})
        else:
            new_salt = self.generate_salt()
            new_passwd = self.createrandompwd(20)
            new_hashed = self.hashing(new_passwd,new_salt)
            previous_hashed = user["new_hashed"]
            user.update(prev_hashed=previous_hashed)
            user.update(new_hashed=new_hashed)
            user.update({"pwd":new_passwd})
        return user

class ServerHandler(object):
    def __init__(self,
                 url=None):
        self.url = url  #must have http:// or https:// prefix

    def ping(self):
        try:
            r = requests.get(self.url+hellopath,timeout=TimeOut)
            if r.status_code != 200:
                return False
        except:
            return False

    def createusers(self,users):
        url = self.url + createpath
        payload = {
            "users": json.dumps(users)
        }
        print('requests ',url,payload) #
        r = requests.post(url,data=payload)
        if r.status_code == 200:
            return True
        else:
            return False

    def removeusersbyhost(self,ipaddr,mac):
        url = self.url + removepath
        payload = {
            "ip": ipaddr,
            "mac": mac
        }
        #print('requests ',url,payload) #
        r = requests.get(url,params=payload,timeout=TimeOut)
        if r.status_code == 200:
            return True
        else:
            return False

    def updateuser(self,user):
        url = self.url + updatepath
        payload = {
            "user": json.dumps(user)
        }
        #print('requests ',url,payload) #
        r = requests.post(url,data=payload,timeout=TimeOut)
        if r.status_code == 200:
            return True
        else:
            return False

    def getusersbyhost(self,ipaddr,mac):
        url = self.url + getpath
        payload = {
            "ip": ipaddr,
            "mac": mac
        }
        #print('requests ',url,payload) #
        r = requests.get(url,params=payload,timeout=TimeOut)
        if r.status_code == 200:
            return r.json()
        else:
            return False



def get_mac_address():
    mac=uuid.UUID(int = uuid.getnode()).hex[-12:]
    return ":".join([mac[e:e+2]for e in range(0,11,2)])

def get_ip():
    myname = socket.getfqdn(socket.gethostname())
    return socket.gethostbyname(myname)

def gethost():
    return get_ip()+get_mac_address()



@route('/change_pwd', method='POST')
def change_pwd():
    user = request.forms.get('user')
    mac = request.forms.get('mac')
    if mac != get_mac_address():
        return HTTPResponse(status=500,body=json.dumps({"msg":"host changed"}))
    if userhandler.change_passwd([json.loads(user)]) == False:
        return HTTPResponse(status=500,body=json.dumps({"msg":"failed to change password"}))
    else:
        return json.dumps({"msg":"OK"})

@route('/rotation_pwd', method='POST')
def rotation_pwd():
    mac = request.forms.get('mac')
    users = request.forms.get('users')
    if mac != get_mac_address():
        return HTTPResponse(status=500,body=json.dumps({"msg":"host changed"}))
    tmp = []
    for u in json.loads(users):
        tmp.append(u)
    if userhandler.change_passwd(tmp) == False:
        return HTTPResponse(status=500,body=json.dumps({"msg":"failed to rotation password"}))
    else:
        return json.dumps({"msg":"OK"})



if __name__ == "__main__":
    args = docopt(__doc__, version="version 0.1")

    app = app()

    url = args['--url']
    if url == None:
        print('Please input Server URL in --url.')
        sys.exit(1)
    userhandler = UserHandler(url)
    if userhandler.startup() == False:
        print('Failed to initialize the service.')
        sys.exit(1)

    run(host=args['--server'],
        port=int(args['--port']),
        app=app,
        reloader=True,
        debug=True)