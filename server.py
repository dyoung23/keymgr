#!/usr/bin/env python

"""
Keymgr:Password changer server for Linux user

To allow users to change their own password without superuser/root privileges.

Usage:
  server.py [-s SERVER -c CLIENTPORT -p port]
  server.py [-h | --help]
  server.py --version

Options:
  -h --help                 Show this screen.
  -v --version              Show version.
  -c --clientport CLIENTPORT  client server port [default: 8001]
  -s --server SERVER        Keymgr server host [default: 0.0.0.0]
  -p --port PORT            Keymgr server port [default: 8002].
"""

import enum
from bottle import run, route, request, app
from docopt import docopt
import json
from client import TimeOut
import etcd3
import requests
import datetime
import crypt
import random
import string

dbhandler = None
etcdhosts = '10.10.110.8'
etcdport = 2379
ca_cert = '/home/dyc/keymgr/ca.pem'
cert_key = '/home/dyc/keymgr/etcd-key.pem'
cert_cert = '/home/dyc/keymgr/etcd.pem'
etcduser = 'root'
etcdpwd = '1qaz@WSX123'
TimeOut = 3

"""
  ____        _        _
 |  _ \  __ _| |_ __ _| |__   __ _ ___  ___
 | | | |/ _` | __/ _` | '_ \ / _` / __|/ _ \
 | |_| | (_| | || (_| | |_) | (_| \__ \  __/
 |____/ \__,_|\__\__,_|_.__/ \__,_|___/\___|
    | | | | __ _ _ __   __| | | ___ _ __
    | |_| |/ _` | '_ \ / _` | |/ _ \ '__|
    |  _  | (_| | | | | (_| | |  __/ |
    |_| |_|\__,_|_| |_|\__,_|_|\___|_|

"""


class DatabaseHandler(object):
    def __init__(self,
                 host=None,
                 port=None,
                 username=None,
                 password=None,
                 **kwargs):
        """
        Class initialization

        Args:
            username:           Username
            previous_passwd:    Current/previous password
            new_passwd:         New password
        """
        self.connection = None
        self.db = None           
        self.userdata = None

    def connect(self):
        pass

    def put(self):
        pass

    def delete(self):
        pass

    def replace(self):
        pass

    def get(self):
        pass



class ETCD3Handler(DatabaseHandler):
    def __init__(self, host=None, port=None, ca_cert=None, cert_key=None, cert_cert=None, user=None, password=None):
        self.host = host
        self.port = port
        self.ca_cert = ca_cert
        self.cert_key = cert_key
        self.cert_cert =cert_cert
        self.user = user
        self.password = password
        self.db = "/keymgr/"   #database or prefix
        self.connection = None

    def connect(self):
        """Connect to etcd3

        """
        print("Connectting ...")
        self.connection = etcd3.client(
            host=self.host,
            port=self.port,
            ca_cert=self.ca_cert,
            cert_key=self.cert_key,
            cert_cert=self.cert_cert#,
            #user=self.user,                
            #password=self.password
        )

    def put(self,values):
        ip = values[0]['ip']
        mac = values[0]['mac']
        key_prefix = self.db + ip + '_' + mac + '/'
        for value in values:
            user = value['user']
            key = key_prefix + user
            print('put %s to key %s' % (value,key)) #
            self.connection.put(key,json.dumps(value))

    def deletebyprefix(self,subprefix):
        key_prefix = self.db + subprefix
        print("delete prefix %s" % key_prefix) #
        self.connection.delete_prefix(key_prefix)

    def replacebyvalue(self,value):
        ip = value['ip']
        mac = value['mac']
        user = value['user']
        key = self.db + ip + '_' + mac + '/' + user
        print('delete key %s' % key) #
        self.connection.delete(key)
        print('and put %s to key %s' % (value,key)) #
        self.connection.put(key,json.dumps(value))

    def getbyprefix(self,subprefix):
        key_prefix = self.db + subprefix
        print('get all value in prefix %s' % key_prefix) #
        accounts = []
        for _,v in enumerate(list(self.connection.get_prefix(key_prefix))):
            accounts.append(json.loads(v[0].decode('utf-8')))
        return accounts

####
def hashing(passwd, salt=None):
    if not salt:
        hashed = crypt.crypt(passwd, str(generate_salt()))
    else:
        hashed = crypt.crypt(passwd, salt)
    return hashed

def generate_salt():
    salt = ([random.choice(string.ascii_letters +
            string.digits) for _ in range(16)])
    salt = "$6$" + ''.join(salt) + "$"
    return salt

def createrandompwd(length=20):
    password = ''.join(
        [random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits + string.punctuation) for i in
         range(length)])
    return password

def userjson(user):
    new_salt = generate_salt()
    new_passwd = createrandompwd(20)
    new_hashed = hashing(new_passwd,new_salt)
    previous_hashed = user["new_hashed"]
    user.update(prev_hashed=previous_hashed)
    user.update(new_hashed=new_hashed)
    user.update({"pwd":new_passwd})
    return user



#curl http://10.10.111.92:8002/hello
@route('/hello')
def hello():
    return json.dumps({"msg":'hello world'})

#
@route('/users/create', method='POST')
def userscreate():
    users = request.forms.get('users')
    print('users', users) #
    tmp = []
    for u in json.loads(users):
        tmp.append(u)
    dbhandler.put(tmp)
    return json.dumps({"msg":"OK"})

#curl http://0.0.0.0:8002/users/remove?ip=10.10.111.92&mac=fa%3A16%3A3e%3A23%3Af8%3Ad4
@route('/users/remove')
def usersremove():
    ip = request.query.ip
    mac = request.query.mac
    print('ip and mac',ip,mac) #
    subprefix = ip + '_' + mac
    dbhandler.deletebyprefix(subprefix)
    return json.dumps({"msg":"OK"})

@route('/users/update', method='POST')
def usersupdate():
    user = request.forms.get('user')
    print('user', user) #
    dbhandler.replacebyvalue(json.loads(user))
    return json.dumps({"msg":"OK"})

#curl http://10.10.111.92:8002/users/get?ip=10.10.111.92&mac=fa:16:3e:23:f8:d4
@route('/users/get')
def usersget():
    ip = request.query.ip
    mac = request.query.mac
    print('ip and mac',ip,mac) #
    if ip == '' or mac == '':
        subprefix = ''
    else:
        subprefix = ip + '_' + mac
    accounts = dbhandler.getbyprefix(subprefix)
    return json.dumps({"data":accounts,"msg":"OK"})

#curl http://10.10.111.92:8002/rotatepwd/10.10.111.92/fa:16:3e:23:f8:d4
@route('/rotatepwd/<ip>/<mac>')
def rotatepwd(ip,mac):
    url = 'http://' + ip + ':' + args['--clientport'] + '/rotation_pwd'
    subprefix = ip + '_' + mac

    users = dbhandler.getbyprefix(subprefix)

    changepwd_users = []
    newusers = []
    for user in users:
        user.update({"update_time":datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d %H:%M:%S')})
        if user['type'] != 'nologin':
            user = userjson(user)
            changepwd_users.append({'user':user['user'],'prev_hashed':user['prev_hashed'],'new_hashed':user["new_hashed"],'type':'login'})
        newusers.append(user)
    
    payload = {
        "mac":mac,
        "users":json.dumps(changepwd_users)
    }
    res = requests.post(url,data=payload,timeout=TimeOut)
    if res.status_code == 200:
        for user in newusers:
            dbhandler.replacebyvalue(user)
        return json.dumps({"msg":"OK"})
    else:
        return json.dumps({"msg":"error"})

#curl -X POST -F user=root http://10.10.111.92:8002/changepwd/10.10.111.92/fa:16:3e:23:f8:d4
@route('/changepwd/<ip>/<mac>', method='POST')
def changepwd(ip,mac):
    user = request.forms.get('user').lower()
    url = 'http://' + ip + ':' + args['--clientport'] + '/change_pwd'
    subprefix = ip + '_' + mac
    key = dbhandler.db + subprefix + '/' + user

    origin_user = dbhandler.connection.get(key)[0].decode('utf-8')
    newuser = userjson(json.loads(origin_user))
    newuser.update({"update_time":datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d %H:%M:%S')})
    
    payload = {
        "user": json.dumps({'user':newuser['user'],'prev_hashed':newuser['prev_hashed'],'new_hashed':newuser["new_hashed"], 'type':'login'}),
        "mac": mac
    }
    res = requests.post(url,data=payload,timeout=TimeOut)
    if res.status_code == 200:
        dbhandler.replacebyvalue(newuser)
        return json.dumps({"msg":"OK"})
    else:
        return json.dumps({"msg":"error"})



if __name__ == "__main__":
    args = docopt(__doc__, version="version 0.1")

    app = app()

    dbhandler = ETCD3Handler(etcdhosts, etcdport, ca_cert, cert_key, cert_cert, etcduser, etcdpwd)
    dbhandler.connect()

    run(host=args['--server'],
        port=int(args['--port']),
        app=app,
        reloader=True,
        debug=True)